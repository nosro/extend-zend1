<?php

namespace OrsDev\ExtendZend1\Lib;


class ExtendZendHttpResponse extends \Zend_Http_Response
{
    /**
     * @inheritdoc
     */
    public function __construct($code, array $headers, $body = null, $version = '1.1', $message = null)
    {
        // Make sure the response code is valid and set it
        if (self::responseCodeAsText($code) === null) {
            #require_once 'Zend/Http/Exception.php';
            throw new \Zend_Http_Exception("{$code} is not a valid HTTP response code");
        }

        $this->code = $code;

        foreach ($headers as $name => $value) {
            if (is_int($name)) {
                $header = explode(":", $value, 2);
                if (count($header) != 2) {
                    #require_once 'Zend/Http/Exception.php';
                    throw new \Zend_Http_Exception("'{$value}' is not a valid HTTP header");
                }

                $name  = trim($header[0]);
                $value = trim($header[1]);
            }

            $this->headers[ucwords(strtolower($name))] = $value;
        }

        // Set the body
        $this->body = $body;

        // Set the HTTP version
        if (! preg_match('|^\d\.\d$|', $version) && ($version != 2)) {
            #require_once 'Zend/Http/Exception.php';
            throw new \Zend_Http_Exception("Invalid HTTP response version: $version");
        }

        $this->version = $version;

        // If we got the response message, set it. Else, set it according to
        // the response code
        if (is_string($message)) {
            $this->message = $message;
        } else {
            $this->message = self::responseCodeAsText($code);
        }
    }

    /**
     * @param $response_str
     * @return ExtendZendHttpResponse
     * @throws \Zend_Http_Exception
     */
    public static function fromStringCustom($response_str)
    {
        $code    = self::extractCode($response_str);
        $headers = static::extractHeaders($response_str);
        $body    = self::extractBody($response_str);
        $version = self::extractVersion($response_str);
        $message = self::extractMessage($response_str);

        return new \OrsDev\ExtendZend1\Lib\ExtendZendHttpResponse($code, $headers, $body, $version, $message);
    }

    /**
     * @inheritdoc
     */
    public static function extractHeaders($response_str)
    {
        $headers = array();

        // First, split body and headers. Headers are separated from the
        // message at exactly the sequence "\r\n\r\n"
        $parts = preg_split('|(?:\r\n){2}|m', $response_str, 2);
        if (! $parts[0]) {
            return $headers;
        }

        // Split headers part to lines; "\r\n" is the only valid line separator.
        $lines = explode("\r\n", $parts[0]);
        unset($parts);
        $last_header = null;

        foreach($lines as $index => $line) {
            if ($index === 0 && preg_match('#^HTTP/2 [1-5]\d+#', $line)) {
                // Status line; ignore
                continue;
            }

            if ($index === 0 && preg_match('#^HTTP/\d+(?:\.\d+) [1-5]\d+#', $line)) {
                // Status line; ignore
                continue;
            }

            if ($line == "") {
                // Done processing headers
                break;
            }

            // Locate headers like 'Location: ...' and 'Location:...' (note the missing space)
            if (preg_match("|^([a-zA-Z0-9\'`#$%&*+.^_\|\~!-]+):\s*(.*)|s", $line, $m)) {
                unset($last_header);
                $h_name  = strtolower($m[1]);
                $h_value = $m[2];
                \Zend_Http_Header_HeaderValue::assertValid($h_value);

                if (isset($headers[$h_name])) {
                    if (! is_array($headers[$h_name])) {
                        $headers[$h_name] = array($headers[$h_name]);
                    }

                    $headers[$h_name][] = ltrim($h_value);
                    $last_header = $h_name;
                    continue;
                }

                $headers[$h_name] = ltrim($h_value);
                $last_header = $h_name;
                continue;
            }

            // Identify header continuations
            if (preg_match("|^[ \t](.+)$|s", $line, $m) && $last_header !== null) {
                $h_value = trim($m[1]);
                if (is_array($headers[$last_header])) {
                    end($headers[$last_header]);
                    $last_header_key = key($headers[$last_header]);

                    $h_value = $headers[$last_header][$last_header_key] . $h_value;
                    \Zend_Http_Header_HeaderValue::assertValid($h_value);

                    $headers[$last_header][$last_header_key] = $h_value;
                    continue;
                }

                $h_value = $headers[$last_header] . $h_value;
                \Zend_Http_Header_HeaderValue::assertValid($h_value);

                $headers[$last_header] = $h_value;
                continue;
            }

            // Anything else is an error condition
            #require_once 'Zend/Http/Exception.php';
            throw new \Zend_Http_Exception('Invalid header line detected');
        }

        return $headers;
    }
}