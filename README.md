# Module Extend Zend1

ver 1.0.0

## Description
- fix issue with HTTP/2 header in zend REST request
- work with stripe payment module
 